" use strict"
    function togglePasswordVisibility(inputId, icon) {
      const passwordInput = document.getElementById(inputId);

      if (passwordInput.type === 'password') {
        passwordInput.type = 'text';
        icon.classList.remove('fa-eye');
        icon.classList.add('fa-eye-slash');
      } else {
        passwordInput.type = 'password';
        icon.classList.remove('fa-eye-slash');
        icon.classList.add('fa-eye');
      }
    }

    function resultCheck(inputId1, inputId2) {
      const password1 = document.getElementById(inputId1).value;
      const password2 = document.getElementById(inputId2).value;

      if (password1 === password2) {
        alert('You are welcome');
      } else {
        const errorText = document.createElement('p');
        errorText.innerText = 'Потрібно ввести однакові значення';
        errorText.style.color = 'red';
        const form = document.querySelector('.password-form');
        form.appendChild(errorText);
      }
    }
  