

class Card {
    constructor(post, user) {
      this.post = post;
      this.user = user;
    }
  
    createCardElement() {
      const cardElement = document.createElement('div');
      cardElement.classList.add('card');
      
      cardElement.innerHTML = `
        <div class="card-header">
          <h2>${this.post.title}</h2>
          <button class="delete-btn">Delete</button>
        </div>
        <div class="card-body">
          <p>${this.post.body}</p>
          <p>Posted by: ${this.user.name} (${this.user.email})</p>
        </div>
      `;
  
      cardElement.querySelector('.delete-btn').addEventListener('click', () => this.deletePost());
  
      return cardElement;
    }
  
    deletePost() {
    
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
        method: 'DELETE'
      })
      .then(response => {
        if (response.ok) {
          
          const cardElement = document.getElementById(`card-${this.post.id}`);
          cardElement.remove();
        } else {
          console.error('Failed to delete post:', response.statusText);
        }
      })
      .catch(error => {
        console.error('Error deleting post:', error);
      });
    }
  }
  
 
  fetch('https://ajax.test-danit.com/api/json/users')
    .then(response => response.json() )
    
    .then(users => {
      return fetch('https://ajax.test-danit.com/api/json/posts')
        .then(response => response.json())
        .then(posts => {
          
          
          displayPosts(posts, users);
        });
        
    })
    
    .catch(error => {
      console.error('Error fetching data:', error);
    });
  
  function displayPosts(posts, users) {
    const postsContainer = document.getElementById('postsContainer');
  
    posts.forEach(post => {
      const user = users.find(user => user.id === post.userId);
      if (user) {
        const card = new Card(post, user);
        const cardElement = card.createCardElement();
        cardElement.id = `card-${post.id}`;
        postsContainer.appendChild(cardElement);
      }
    });
  }
  

  