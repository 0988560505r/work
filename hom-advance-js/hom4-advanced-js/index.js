




const url = 'https://ajax.test-danit.com/api/swapi/films';


const xhr = new XMLHttpRequest();
xhr.open('GET', url, true);

xhr.onload = function() {
  if (xhr.status >= 200 && xhr.status < 300) {
    const films = JSON.parse(xhr.responseText);
    console.log("Список фільмів", films);

    
    processFilms(films);

  } else {
    console.error('Сталася помилка при отриманні даних про фільми:', xhr.statusText);
  }
};

xhr.onerror = function() {
  console.error('Сталася помилка при виконанні запиту.');
};

xhr.send();


function processFilms(films) {
  
  const filmsList = document.getElementById('filmsList');

  const sortedFilms = films.sort((a, b) => {
    const episodeIdA = a.episode_id || a.episodeId;
    const episodeIdB = b.episode_id || b.episodeId;
    return episodeIdA - episodeIdB;
  });

  sortedFilms.forEach(film => {
    const listItem = document.createElement('li');
    const episodeId = film.episode_id || film.episodeId;
    const name = film.title || film.name;
    const openingCrawl = film.opening_crawl || film.openingCrawl;

    listItem.innerHTML = `
      <strong>Епізод ${episodeId}: ${name}</strong>
      <p>${openingCrawl}</p>
    `;
    filmsList.appendChild(listItem);
  });
}

function processFilms(films) {
  const filmsList = document.getElementById('filmsList');

  const sortedFilms = films.sort((a, b) => {
    const episodeIdA = a.episode_id || a.episodeId;
    const episodeIdB = b.episode_id || b.episodeId;
    return episodeIdA - episodeIdB;
  });

  sortedFilms.forEach(film => {
    const listItem = document.createElement('li');
    const episodeId = film.episode_id || film.episodeId;
    const name = film.title || film.name;
    const openingCrawl = film.opening_crawl || film.openingCrawl;

    listItem.innerHTML = `
      <strong>Епізод ${episodeId}: ${name}</strong>
      <p>${openingCrawl}</p>
      <ul id="characters_${episodeId}"></ul> <!-- Додали список персонажів -->
    `;
    filmsList.appendChild(listItem);

    
    film.characters.forEach(characterUrl => {
      fetch(characterUrl)
        .then(response => response.json())
        .then(character => {
          const charactersList = document.getElementById(`characters_${episodeId}`);
          const characterItem = document.createElement('li');
          characterItem.textContent = character.name;
          charactersList.appendChild(characterItem);
        })
        .catch(error => {
          console.error('Помилка при отриманні даних про персонажа:', error);
        });
    });
  });
}
