




const findBtn = document.getElementById("find-btn");
const infoDiv = document.getElementById("info");

async function getIpAddress() {
    const response = await fetch("https://api.ipify.org/?format=json");
    const data = await response.json();
    return data.ip;
}

async function getLocationInfo(ip) {
    const response = await fetch(`http://ip-api.com/json/${ip}`);
    const data = await response.json();
    return data;
}

async function showInfo() {
    try {
        const ipAddress = await getIpAddress();
        const locationInfo = await getLocationInfo(ipAddress);
    
        infoDiv.innerHTML = `
            <p>IP-адреса: ${ipAddress}</p>
            <p>Континент: ${locationInfo.timezone}</p>
            <p>Країна: ${locationInfo.country}</p>
            <p>Регіон: ${locationInfo.regionName}</p>
            <p>Місто: ${locationInfo.city}</p>
            <p>Район: (${locationInfo.lat}, ${locationInfo.lon})</p>
        `;
    } catch (error) {
        console.error("Помилка отримання даних:", error);
        infoDiv.innerHTML = "<p>Виникла помилка під час отримання даних.</p>";
    }
}

findBtn.addEventListener("click", showInfo);
