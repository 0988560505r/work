


const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

function renderBooks(books) {
  const rootElement = document.getElementById("root");
  const ulElement = document.createElement("ul");

  books.forEach(book => {
    try {
      if (!book.author) {
        throw new Error("В об'єкті відсутня властивість 'author'");
      }
      if (!book.name) {
        throw new Error("В об'єкті відсутня властивість 'name'");
      }
      if (!book.price) {
        throw new Error("В об'єкті відсутня властивість 'price'");
      }
      
      const liElement = document.createElement("li");
      liElement.textContent = `${book.author}, ${book.name}, ${book.price} грн`;
      ulElement.appendChild(liElement);
    } catch (error) {
      console.error("Помилка: ", error.message);
    }
  });

  rootElement.appendChild(ulElement);
}

renderBooks(books);