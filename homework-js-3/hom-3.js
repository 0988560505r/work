"use strict"

const userInput = prompt("Введите число:");
if (userInput !== null) {
const number = parseInt(userInput);

  if (!isNaN(number)) {
      const multiplesOfFive = [];

     for (let i = 0; i <= number; i++) {
      
      if (i % 5 === 0) {
       multiplesOfFive.push(i);
      }
    }
if (multiplesOfFive.length > 0) {
      
    console.log("Число, кратные 5:", multiplesOfFive);
    } else {
      console.log("Sorry, no numbers");
    }
  } else {
    console.log("Введено число неправельно");
  }
} 
