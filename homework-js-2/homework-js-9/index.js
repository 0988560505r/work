"use strict"
function displayList(array, parent = document.body) {
    
    const list = document.createElement('ul');
  
    
    for (let i = 0; i < array.length; i++) {
      
      const listItem = document.createElement('li');
      
      listItem.textContent = array[i];
      
      list.appendChild(listItem);
    }
  
    
    parent.appendChild(list);
  }
  const array1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
  displayList(array1);
  
   