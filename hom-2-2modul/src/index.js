
const menuButton = document.getElementById("menuButton");
const menuIcon1 = document.getElementById("menuIcon1");
const menuIcon2 = document.getElementById("menuIcon2");
const menuNav = document.getElementById("menuNav");


menuButton.addEventListener("click", function () {
   
    if (menuNav.style.display === "none" || menuNav.style.display === "") {
       
        menuNav.style.display = "block";
        menuIcon1.style.display = "none";
        menuIcon2.style.display = "block";
    } else {
        
        menuNav.style.display = "none";
        menuIcon1.style.display = "block";
        menuIcon2.style.display = "none";
    }
});