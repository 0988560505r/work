"use strict"
const tabTitles = document.querySelectorAll('.tabs-title');
    const tabContents = document.querySelectorAll('.tabs-content');

    tabTitles.forEach(title => {
        title.addEventListener('click', () => {
            const tabName = title.getAttribute('data-tab');
            
            tabContents.forEach(content => {
                const contentName = content.getAttribute('data-tab-content');
                if (contentName === tabName) {
                    content.classList.add('active');
                } else {
                    content.classList.remove('active');
                }
            });

            tabTitles.forEach(t => {
                const tName = t.getAttribute('data-tab');
                if (tName === tabName) {
                    t.classList.add('active');
                } else {
                    t.classList.remove('active');
                }
            });
        });
    });