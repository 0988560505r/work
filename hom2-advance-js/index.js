
const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

function isValidBook(book) {
  if (!book.author) {
    console.error("Missing author:", book);
    return false;
  }
  if (!book.name) {
    console.error("Missing name:", book);
    return false;
  }
  if (!book.price) {
    console.error("Missing price:", book);
    return false;
  }
  return true;
}

function createListElement(book) {
  const li = document.createElement("li");
  li.textContent = `${book.name} - ${book.author}, Price: ${book.price}`;
  return li;
}

function renderBooks(books) {
  const root = document.getElementById("root");
  const ul = document.createElement("ul");

  books.forEach(book => {
    if (isValidBook(book)) {
      const li = createListElement(book);
      ul.appendChild(li);
    }
  });

  root.appendChild(ul);
}

renderBooks(books);