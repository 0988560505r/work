
class Employee {
    constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }
  
    
    get name() {
      return this._name;
    }
  
    set name(name) {
      this._name = name;
    }
  
    get age() {
      return this._age;
    }
  
    set age(age) {
      this._age = age;
    }
  
    get salary() {
      return this._salary;
    }
  
    set salary(salary) {
      this._salary = salary;
    }
  }
  
  
  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this._lang = lang;
    }
  
    
    get salary() {
      return this._salary * 3; 
    }
  
    
    get lang() {
      return this._lang;
    }
  }
  
 
  const programmer1 = new Programmer("John", 30, 50, ["JavaScript", "Python"]);
  const programmer2 = new Programmer("Alice", 25, 70, ["Java", "C++"]);
  

  console.log("Информация о программисте 1:", programmer1.name, programmer1.age, programmer1.salary, programmer1.lang);
  console.log("Информация о программисте 2:", programmer2.name, programmer2.age, programmer2.salary,programmer2.lang);
  
  
