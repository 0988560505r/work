"use strict"

 function filterBy(array, data_type) {
    return array.filter(item => typeof item !== data_type);
  }
  let array = ['hello', 'world', 23, '23', null];
  let filteredArray = filterBy(array, 'string');
  console.log(filteredArray);
    