"use strict"
function createNewUser() {
  let firstName = prompt("Укажите ваше имя:","Имя");
  let lastName = prompt("Укажите вашу фамилию:","Фамилия");

  let newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin: function() {
      return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
    }
  };

  return newUser;
}

let user = createNewUser();
console.log(user.getLogin());