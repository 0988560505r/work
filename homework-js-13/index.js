"use strict"
        const images = document.querySelectorAll('.image-to-show');
        const startButton = document.getElementById('startButton');
        const stopButton = document.getElementById('stopButton');
        const resumeButton = document.getElementById('resumeButton');
        let currentIndex = 0;
        let intervalId;

        function showImage(index) {
            images.forEach((image, i) => {
                if (i === index) {
                    image.style.display = 'block';
                } else {
                    image.style.display = 'none';
                }
            });
        }

        function startSlideshow() {
            showImage(currentIndex);
            intervalId = setInterval(() => {
                currentIndex = (currentIndex + 1) % images.length;
                showImage(currentIndex);
            }, 3000);

            startButton.disabled = true;
            stopButton.disabled = false;
            resumeButton.disabled = true;
        }

        function stopSlideshow() {
            clearInterval(intervalId);
            stopButton.disabled = true;
            resumeButton.disabled = false;
        }

        function resumeSlideshow() {
            startSlideshow();
        }

        startButton.addEventListener('click', startSlideshow);
        stopButton.addEventListener('click', stopSlideshow);
        resumeButton.addEventListener('click', resumeSlideshow);
    