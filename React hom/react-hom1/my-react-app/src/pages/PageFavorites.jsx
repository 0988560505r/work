








import React from 'react';
import CartCounter from '../componetc/counter/CartCounter';
import FavoriteCounter from '../componetc/counter/FavoriteCounter';
import ProductList from '../componetc/product/ProductList';
import './style.scss';

const PageFavorites = ({ 
    favoriteCount, 
    products, 
    addToCartHandler, 
    cartCount, 
    addToFavoriteHandler, 
    setFavoriteCount }) => {
    const handleRemoveFromFavorite = (product) => {
        setFavoriteCount(prevCount => prevCount.filter(item => item.article !== product.article));
    };
console.log(setFavoriteCount);
    return (
        <div className="page-favorites">
            <ProductList 
                products={favoriteCount} // Здесь передаем список избранных продуктов
                onAddToCart={addToCartHandler}
                onAddToFavorite={addToFavoriteHandler}
                onRemoveFromFavorite={handleRemoveFromFavorite} // Добавляем обработчик удаления из избранного
                favorites={favoriteCount}
                isFavoritesPage={true}
                isCartPage={false}
                isHomePage={false}
            />

            <CartCounter count={cartCount.length} />
            <FavoriteCounter count={favoriteCount.length} />
        </div>
    );
};

export default PageFavorites;
