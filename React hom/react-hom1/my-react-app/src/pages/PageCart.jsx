// import React, {useState} from 'react';
// import CartCounter from '../componetc/counter/CartCounter';
// import FavoriteCounter from '../componetc/counter/FavoriteCounter';
// import './style.scss';
// import ProductList from '../componetc/product/ProductList';



// const PageCart = ({
//     favoriteCount,
//     products,
// addToCartHandler,
// cartCount,
// addToFavoriteHandler,
// onRemoveFromCart }) => {
//     const handleRemoveFromCart = (product) => {
//         onRemoveFromCart(prevCount => prevCount.filter(item => item.article !== product.article));
//     };
//         console.log(cartCount , 'products');
//     return (
        
//     <div>

//     <ProductList
//                 favorites={favoriteCount}
//                 products={cartCount}
//                 onAddToCart={addToCartHandler}
//                 onAddToFavorite={addToFavoriteHandler}
//                 isCartPage={true} // Указываем, что это страница корзины
//                 isFavoritesPage={true}
//                 onRemoveFromCart={handleRemoveFromCart} 
//             />
//             <CartCounter count={cartCount.length} />
//             <FavoriteCounter count={favoriteCount.length} />
// </div>
   
//     )
//     };

// export default PageCart;




import React from 'react';
import CartCounter from '../componetc/counter/CartCounter';
import FavoriteCounter from '../componetc/counter/FavoriteCounter';
import './style.scss';
import ProductList from '../componetc/product/ProductList';

const PageCart = ({
    favoriteCount,
    products,
    addToCartHandler,
    cartCount,
    addToFavoriteHandler,
    setCartCount
}) => {
    const handleRemoveFromCart = (product) => {
        setCartCount(prevCount => prevCount.filter(item => item.article !== product.article));
    };
    console.log(cartCount, 'products');
    return (
        <div>
            <ProductList
                favorites={favoriteCount}
                products={cartCount}
                onAddToCart={addToCartHandler}
                onAddToFavorite={addToFavoriteHandler}
                isCartPage={true}
                isFavoritesPage={false} // Убедитесь, что это значение установлено правильно
                onRemoveFromCart={handleRemoveFromCart} 
            />
            <CartCounter count={cartCount.length} />
            <FavoriteCounter count={favoriteCount.length} />
        </div>
    );
};

export default PageCart;

