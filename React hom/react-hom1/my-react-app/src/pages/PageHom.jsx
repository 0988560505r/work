





    
import React, { useState, useEffect } from 'react';
import './style.scss';
import ProductList from '../componetc/product/ProductList';
import CartCounter from '../componetc/counter/CartCounter';
import FavoriteCounter from '../componetc/counter/FavoriteCounter';

const PageHom = (
   {favoriteCount,
    products,
    addToCartHandler,
    cartCount,
    addToFavoriteHandler,
    
} ) => {


    return (
        <div className="divPageHom">
            {/* <div className="ok">good day PageHom</div> */}
            <ProductList
                favorites={favoriteCount}
                products={products}
                onAddToCart={addToCartHandler}
                onAddToFavorite={addToFavoriteHandler}

     
            />

            <CartCounter count={cartCount.length} />
            <FavoriteCounter count={favoriteCount.length} />
        </div>
    );
};

export default PageHom;

    
    


// import React from 'react';
// import CartCounter from '../componetc/counter/CartCounter';
// import FavoriteCounter from '../componetc/counter/FavoriteCounter';
// import './style.scss';
// import ProductList from '../componetc/product/ProductList';

// const PageHom = ({ favoriteCount, products, addToCartHandler, cartCount, addToFavoriteHandler }) => {
//     return (
//         <div className="divPageHom">
//             <ProductList
//                 favorites={favoriteCount}
//                 products={products}
//                 onAddToCart={addToCartHandler}
//                 onAddToFavorite={addToFavoriteHandler}
//                 isCartPage={false}
//                 isFavoritesPage={false}
//                 isHomePage={true}
//             />
//             <CartCounter count={cartCount.length} />
//             <FavoriteCounter count={favoriteCount.length} />
//         </div>
//     );
// };

// export default PageHom;


