





import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Header from './componetc/header/Header.jsx';
import AppRouter from './routers/index.jsx';
import CartCounter from './componetc/counter/CartCounter.jsx';
import FavoriteCounter from './componetc/counter/FavoriteCounter.jsx';
import PageHom from './pages/PageHom.jsx'; // Импортируем обновленную страницу
import ProductList from './componetc/product/ProductList.jsx';
import PageCart from './pages/PageCart.jsx';

const App = () => {
    return (
        
            <div>
                
                <Header />
              
                <AppRouter />
                
                
                
            </div>
        
    );
};

export default App;





