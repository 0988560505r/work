

import React, { useState,useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';

import PageCart from '../pages/PageCart'; // Імпортуємо компонент PageCart
import PageFavorites from '../pages/PageFavorites';
import PageHome from '../pages/PageHom';

const AppRouter = () => {
    const [products, setProducts] = useState([]);
    const [cartCount, setCartCount] = useState(() => {
        const cart = localStorage.getItem('cart');
        return cart ? JSON.parse(cart) : [];
    });
    const [favoriteCount, setFavoriteCount] = useState(() => {
        const favorite = localStorage.getItem('favorite');
        return favorite ? JSON.parse(favorite) : [];
    });

    const fetchProducts = async () => {
        try {
            const response = await fetch('/products.json');
            if (!response.ok) {
                throw new Error('Failed to fetch products');
            }
            const data = await response.json();
            setProducts(data);
        } catch (error) {
            console.error('Error fetching products:', error);
        }
    };

    useEffect(() => {
        fetchProducts();
    }, []);

    useEffect(() => {
        localStorage.setItem('favorite', JSON.stringify(favoriteCount));
    }, [favoriteCount]);

    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cartCount));
    }, [cartCount]);

    const addToCartHandler = (item) => {
        setCartCount(prevCount => [...prevCount, item]);
    };

    const addToFavoriteHandler = (item) => {
        setFavoriteCount(prevCount => [...prevCount, item]);
    };
    
    return (
        <Routes>
 <Route path="/" element={<PageHome 
  favoriteCount={favoriteCount} 
  products={products} 
  addToCartHandler={addToCartHandler} 
  cartCount={cartCount} 
  addToFavoriteHandler={addToFavoriteHandler} 
/>}/>
            <Route path="/cart" element={<PageCart 
  favoriteCount={favoriteCount} 
  products={products} 
  addToCartHandler={addToCartHandler} 
  cartCount={cartCount} 
  addToFavoriteHandler={addToFavoriteHandler} 
  setCartCount={setCartCount}
//   isCartPage={isCartPage}
  />} /> {/* Додаємо маршрут для /cart */}
            <Route path="/favorite" element={<PageFavorites
                favoriteCount={favoriteCount} 
                products={products} 
                addToCartHandler={addToCartHandler} 
                cartCount={cartCount} 
                addToFavoriteHandler={addToFavoriteHandler}
                setFavoriteCount={setFavoriteCount} // Передаем setFavoriteCount
            />} />
            {/* Додайте інші маршрути тут */}
        </Routes>
    )
}

export default AppRouter;



