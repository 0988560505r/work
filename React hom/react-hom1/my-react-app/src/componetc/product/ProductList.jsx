











// import React, { useState } from 'react';
// import PropTypes from 'prop-types';
// import Button from '../button/Button.jsx';
// import './ProductStyle.scss';
// import ModalImage from '../modal/ModalImage.jsx';
// import ModalText from '../modal/ModalText.jsx';
// import RemoveConfirmationModal from '../modal/RemoveConfirmationModal.jsx';
// import ProductCard from './ProductCard.jsx';

// const ProductList = ({ 
//     products, 
//     onAddToCart, 
//     onAddToFavorite, 
//     onRemoveFromCart, 
//     onRemoveFromFavorite, // Добавляем обработчик удаления из избранного
//     favorites, 
//     isFavoritesPage, 
//     isCartPage,
//     // isHomePage
// }) => {
//     const [showModalImage, setShowModalImage] = useState(false);
//     const [showModalText, setShowModalText] = useState(false);
//     const [selectedProduct, setSelectedProduct] = useState({});
//     const [showRemoveModal, setShowRemoveModal] = useState(false);
//     const [selectedProductForRemove, setSelectedProductForRemove] = useState(null);

//     const handleOpenModalImage = (product) => {
//         setSelectedProduct(product);
//         setShowModalImage(true);
//     };

//     const handleOpenModalText = (product) => {
//         setSelectedProduct(product);
//         setShowModalText(true);
//     };

//     const handleCloseModal = () => {
//         setShowModalImage(false);
//         setShowModalText(false);
//     };

//     const handleOpenRemoveModal = (product) => {
//         setSelectedProductForRemove(product);
//         setShowRemoveModal(true);
//     };

//     const handleCloseRemoveModal = () => {
//         setShowRemoveModal(false);
//     };

   

//     const handleFavoriteClick = (product) => {
//         if (isFavoritesPage) {
//             onRemoveFromFavorite(product);
//         } else {
//             handleOpenModalText(product);
//         }
//     };
//     const handleRemoveFromCart = (product) => {
//         onRemoveFromCart(product);
//         handleCloseRemoveModal();
//     };
//     console.log(showModalImage);
//     return (
//         <div className="product-list">
//             {products.map((product, index) => {
//                 return (
//                     <ProductCard
//                         key={index}
//                         product={product}
//                         onOpenModalImage={handleOpenModalImage}
//                         onOpenModalText={handleOpenModalText}
//                         onOpenRemoveModal={handleOpenRemoveModal}
//                         favorites={favorites}
//                         isCartPage={isCartPage}
//                         isFavoritesPage={isFavoritesPage}
//                         onRemoveFromFavorite={onRemoveFromFavorite} 
//                        // Передаем функцию удаления из избранного
                    
//                     />

//                 );
//             })}
            
//             {showModalImage && selectedProduct && (
//                 <ModalImage product={selectedProduct} onClose={handleCloseModal} onAddToCart={() => onAddToCart(selectedProduct)} />
//             )}
//             {showModalText && (
//                 <ModalText product={selectedProduct} onAddToFavorite={() => onAddToFavorite(selectedProduct)} onClose={handleCloseModal} />
//             )}
//             {showRemoveModal && (
//                 <RemoveConfirmationModal product={selectedProductForRemove} onClose={handleCloseRemoveModal} onRemove={handleRemoveFromCart} />
//             )}
//         </div>
//     );
// };

// ProductList.propTypes = {
//     products: PropTypes.array.isRequired,
//     onAddToCart: PropTypes.func.isRequired,
//     onAddToFavorite: PropTypes.func.isRequired,
//     onRemoveFromCart: PropTypes.func.isRequired,
    
//     onRemoveFromFavorite: PropTypes.func.isRequired, // Добавляем пропс для обработчика удаления из избранного
//     favorites: PropTypes.array.isRequired,
//     isFavoritesPage: PropTypes.bool.isRequired,
//     isCartPage: PropTypes.bool.isRequired,
//     isHomePage: PropTypes.bool.isRequired,
// };

// export default ProductList;





import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Button from '../button/Button.jsx';
import './ProductStyle.scss';
import ModalImage from '../modal/ModalImage.jsx';
import ModalText from '../modal/ModalText.jsx';
import RemoveConfirmationModal from '../modal/RemoveConfirmationModal.jsx';
import ProductCard from './ProductCard.jsx';

const ProductList = ({
    products,
    onAddToCart,
    onAddToFavorite,
    onRemoveFromCart,
    onRemoveFromFavorite,
    favorites,
    isFavoritesPage,
    isCartPage,
}) => {
    const [showModalImage, setShowModalImage] = useState(false);
    const [showModalText, setShowModalText] = useState(false);
    const [selectedProduct, setSelectedProduct] = useState({});
    const [showRemoveModal, setShowRemoveModal] = useState(false);
    const [selectedProductForRemove, setSelectedProductForRemove] = useState(null);

    const handleOpenModalImage = (product) => {
        setSelectedProduct(product);
        setShowModalImage(true);
    };

    const handleOpenModalText = (product) => {
        setSelectedProduct(product);
        setShowModalText(true);
    };

    const handleCloseModal = () => {
        setShowModalImage(false);
        setShowModalText(false);
    };

    const handleOpenRemoveModal = (product) => {
        setSelectedProductForRemove(product);
        setShowRemoveModal(true);
    };

    const handleCloseRemoveModal = () => {
        setShowRemoveModal(false);
    };

    const handleRemoveFromCart = (product) => {
        onRemoveFromCart(product);
        handleCloseRemoveModal();
    };

    return (
        <div className="product-list">
            {products.map((product, index) => (
                <ProductCard
                    key={index}
                    product={product}
                    onOpenModalImage={handleOpenModalImage}
                    onOpenModalText={handleOpenModalText}
                    onOpenRemoveModal={handleOpenRemoveModal}
                    favorites={favorites}
                    isCartPage={isCartPage}
                    isFavoritesPage={isFavoritesPage}
                    onRemoveFromFavorite={onRemoveFromFavorite}
                    onRemoveFromCart={onRemoveFromCart}
                />
            ))}
            {showModalImage && selectedProduct && (
                <ModalImage product={selectedProduct} onClose={handleCloseModal} onAddToCart={() => onAddToCart(selectedProduct)} />
            )}
            {showModalText && (
                <ModalText product={selectedProduct} onAddToFavorite={() => onAddToFavorite(selectedProduct)} onClose={handleCloseModal} />
            )}
            {showRemoveModal && (
                <RemoveConfirmationModal product={selectedProductForRemove} onClose={handleCloseRemoveModal} onRemove={handleRemoveFromCart} />
            )}
        </div>
    );
};

ProductList.propTypes = {
    products: PropTypes.array.isRequired,
    onAddToCart: PropTypes.func.isRequired,
    onAddToFavorite: PropTypes.func.isRequired,
    onRemoveFromCart: PropTypes.func.isRequired,
    onRemoveFromFavorite: PropTypes.func.isRequired,
    favorites: PropTypes.array.isRequired,
    isFavoritesPage: PropTypes.bool.isRequired,
    isCartPage: PropTypes.bool.isRequired,
};

export default ProductList;

