







import React from 'react';
import PropTypes from 'prop-types';
import Button from '../button/Button.jsx';
import './ProductStyle.scss';
import { useLocation } from 'react-router-dom';

const ProductCard = ({ product, onOpenModalImage, onOpenModalText, onOpenRemoveModal, favorites, 
    isCartPage, isFavoritesPage, onRemoveFromFavorite }) => {
    const { image, name, price, article, color, basket } = product;
const location = useLocation()
    const handleAddToFavorite = () => {
        onOpenModalText(product);
    };
    const handleRemoveFromFavorite = () => {
        onRemoveFromFavorite(product);
    };
    console.log("isFavoritesPage", isFavoritesPage);
    const isFavorite = favorites.some((item) => String(item.article) === String(article));
const IS_CART=location.pathname ==='/cart'

const clickFavorites = () => {isFavoritesPage ? handleRemoveFromFavorite() :
    handleAddToFavorite()  
    }
    const clickKart = () => {
    onOpenRemoveModal(product);
    }
const handleClick= IS_CART ? clickKart  : clickFavorites;
    return (
        <div className="product-card">
            <div className="button-container">
                {!isCartPage && !isFavoritesPage && ( // Условие для отображения кнопки с корзиной
                    <Button onClick={() => {
                      console.log("onOpenModalImage" );
                        
                        onOpenModalImage(product)
                        
                        }}>
                        <img src={basket} alt="Basket" className="button-icon" />
                    </Button>
                )}
                <Button onClick={handleClick}> 
                
                    <img 
                        src={isCartPage ? "/close.png" : (isFavorite ? "/like2.png" : "/like.png")} 
                        alt={isCartPage ? "Close" : isFavorite ? "Favorite" : "Not favorite"} 
                        className="button-icon" 
                    />
                </Button>
                
            </div>
            <img src={image} alt={name} />
            <div className="product-details">
                <p><strong>Name: </strong>{name}</p>
                <p><strong>Price: </strong>${price}</p>
                <p><strong>Article: </strong>{article}</p>
                <p><strong>Color: </strong>{color}</p>
            </div>
        </div>
    );
};

ProductCard.propTypes = {
    product: PropTypes.shape({
        image: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        article: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
        color: PropTypes.string.isRequired,
        basket: PropTypes.string.isRequired,
    }).isRequired,
    onOpenModalImage: PropTypes.func.isRequired,
    onOpenModalText: PropTypes.func.isRequired,
    onOpenRemoveModal: PropTypes.func.isRequired,
    favorites: PropTypes.array.isRequired,
    isCartPage: PropTypes.bool.isRequired,
    isFavoritesPage: PropTypes.bool.isRequired,
    onRemoveFromFavorite: PropTypes.func.isRequired, // Добавляем пропс для удаления из избранного
};

export default ProductCard;





