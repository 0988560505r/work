import React, { useState } from 'react';
import PropTypes from "prop-types";
import './Modal.scss';


function ModalBody({ children }) {
  return (
    <div className="modal-body">
      {children}
    </div>
  );
}

ModalBody.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ModalBody;
