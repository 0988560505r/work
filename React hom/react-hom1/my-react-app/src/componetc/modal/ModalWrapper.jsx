




import React from 'react';
import PropTypes from 'prop-types';
import './Modal.scss'; // Переконайтеся, що шлях до вашого файлу стилів вказаний вірно

const ModalWrapper = ({ children,onClose }) => {
  // Функція для закриття модального вікна при натисканні на врапер
  const handleCloseModal = (event) => {
    // Перевіряємо, чи було натиснута саме врапер, а не дочірні елементи
    if (event.target === event.currentTarget) {
      onClose();
    }
  };

  // Визначаємо клас врапера залежно від того, чи відкрите модальне вікно
  const wrapperClassName = 'modal-wrapper';

  return (
    <div className={wrapperClassName} onClick={handleCloseModal}>
      {children}
    </div>
  );
};

ModalWrapper.propTypes = {
  
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ModalWrapper;
