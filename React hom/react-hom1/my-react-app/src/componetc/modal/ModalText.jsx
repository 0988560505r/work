
import React from 'react';
import PropTypes from 'prop-types';
import ModalClose from '../modal/ModalClose.jsx';
import ModalBody from '../modal/ModalBody.jsx';
import ModalFooter from '../modal/ModalFooter.jsx';
import ModalWrapper from './ModalWrapper.jsx';
import './Modal.scss'; // Подключаем стили модального окна
import ModalHeader from './ModalHeader.jsx';
import PageFavorites from '../../pages/PageFavorites.jsx';


function ModalText({ product, onClose,   onAddToFavorite
 }) {
  const handlePrimaryClick = () => {
   
    onAddToFavorite();
    onClose();
  };

  return (
    <ModalWrapper onClose={onClose}>
      <div className="modalText">
        <div className='div-ModalClose'>
          <ModalClose onClose={onClose} /> {/* Кнопка закрытия */}
        </div>
        <ModalHeader imageUrl={product.image} />
        <div className="modal-content">
          <ModalBody>
            <h2 className='modal-body1 mod2-h'>Add Product “NAME”</h2>
            <p className='modal-body2 mod2-p'>Description for your product</p>
          </ModalBody>
          <ModalFooter 
            firstText="Добавить в избранное" 
            firstClick={handlePrimaryClick} 
            closeModal={onClose} 
          />
        </div>
      </div>
    </ModalWrapper>
  );
}

ModalText.propTypes = {

  onClose: PropTypes.func.isRequired,
  onAddToFavorite: PropTypes.func.isRequired,
  
};

export default ModalText;










