



import React from 'react';
import PropTypes from "prop-types";
import './Modal.scss';

function ModalHeader({ imageUrl }) {
  return (
    <div className='div-ModalHeader'>
        {imageUrl && <img src={imageUrl} alt="Product" className='product-image'/>}
    </div>
  );
}

ModalHeader.propTypes = {
  imageUrl: PropTypes.string, // Сделать пропс imageUrl необязательным
};

export default ModalHeader;
