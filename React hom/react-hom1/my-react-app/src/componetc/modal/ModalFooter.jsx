



// import React from 'react';
// import PropTypes from 'prop-types';

// function ModalFooter({ firstText, secondText, firstClick, secondClick, closeModal }) {
//   const handleFirstButtonClick = () => {
//     firstClick();
//     closeModal();
//   };

//   const handleSecondButtonClick = () => {
//     secondClick();
//     closeModal();
//   };

//   return (
//     <div className="modal-footer">
//       {firstText && (
//         <button className='button1-ModalImage modal2Bnt' onClick={handleFirstButtonClick}>{firstText}</button>
//       )}
//       {secondText && (
//         <button className='button1-ModalImage2' onClick={handleSecondButtonClick}>{secondText}</button>
//       )}
//     </div>
//   );
// }

// ModalFooter.propTypes = {
//   firstText: PropTypes.string,
//   secondText: PropTypes.string,
//   firstClick: PropTypes.func,
//   secondClick: PropTypes.func,
//   closeModal: PropTypes.func.isRequired,
// };

// export default ModalFooter;





import React from 'react';
import PropTypes from 'prop-types';

function ModalFooter({ firstText, secondText, firstClick, secondClick, closeModal }) {
    const handleFirstButtonClick = () => {
        firstClick();
        closeModal('firstButtonClick');
    };

    const handleSecondButtonClick = () => {
        secondClick();
        closeModal('secondButtonClick');
    };

    return (
        <div className="modal-footer">
            {firstText && (
                <button className='button1-ModalImage modal2Bnt' onClick={handleFirstButtonClick}>{firstText}</button>
            )}
            {secondText && (
                <button className='button1-ModalImage2' onClick={handleSecondButtonClick}>{secondText}</button>
            )}
        </div>
    );
}

ModalFooter.propTypes = {
    firstText: PropTypes.string,
    secondText: PropTypes.string,
    firstClick: PropTypes.func,
    secondClick: PropTypes.func,
    closeModal: PropTypes.func.isRequired,
};

export default ModalFooter;
