




// import React from 'react';
// import PropTypes from 'prop-types';
// import Button from '../button/Button';
//  import ModalWrapper from './ModalWrapper'; // Убедитесь, что путь к вашему компоненту ModalWrapper указан правильно
// import './Modal.scss'; // Убедитесь, что путь к вашему файлу стилей указан правильно


// const RemoveConfirmationModal = ({ product, onClose, onRemove }) => {
//     const handleCloseModal = (event) => {
//         if (event.target === event.currentTarget) {
//             onClose();
//         }
//     };

//     return (
//         <ModalWrapper onClose={onClose}>
//             <div className='div-modal'>
//             <div className="modal-wrapper1" onClick={handleCloseModal}>
//                 <img src={product.image} alt={product.name} />
//                 <p>Delete Product "{product.name}"</p>
//                 <p>Description for your product</p>
//                 <button onClick={onClose}>Don't Remove</button>
//                 <button onClick={() => onRemove(product)}>Remove</button>
//             </div>
//             </div>
//         </ModalWrapper>
//     );
// };

// RemoveConfirmationModal.propTypes = {
//     product: PropTypes.object.isRequired,
//     onClose: PropTypes.func.isRequired,
//     onRemove: PropTypes.func.isRequired,
// };

// export default RemoveConfirmationModal;




import React from 'react';
import PropTypes from 'prop-types';
import ModalWrapper from './ModalWrapper';
import './Modal.scss';

const RemoveConfirmationModal = ({ product, onClose, onRemove }) => {
    const handleCloseModal = (event) => {
        if (event.target === event.currentTarget) {
            onClose();
        }
    };

    return (
        <ModalWrapper onClose={onClose}>
            <div className='div-modal'>
                <div className="modal-wrapper1" onClick={handleCloseModal}>
                    <img src={product.image} alt={product.name} />
                    <p className='pText'> Удалить продукт : "{product.name} ?"</p>
                    {/* <p>Description for your product</p> */}
                    <button className='btnDontRemove' onClick={onClose}>Не удалять</button>
                    <button className='btnRemove ww' onClick={() => onRemove(product)}>Удалить</button>
                </div>
            </div>
        </ModalWrapper>
    );
};

RemoveConfirmationModal.propTypes = {
    product: PropTypes.object.isRequired,
    onClose: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
};

export default RemoveConfirmationModal;
