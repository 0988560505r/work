import React, { useState } from 'react';
import PropTypes from "prop-types";
import './Modal.scss';



function ModalClose({ onClose }) {
  return (
    <img src="./close.png" alt="Close" className="close-icon" onClick={onClose} />
  );
}

ModalClose.propTypes = {
  onClose: PropTypes.func.isRequired,
};

export default ModalClose;
