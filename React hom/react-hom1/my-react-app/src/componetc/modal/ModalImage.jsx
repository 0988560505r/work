




// import React from 'react';
// import PropTypes from 'prop-types';
// import ModalFooter from '../modal/ModalFooter.jsx';
// import ModalClose from '../modal/ModalClose.jsx'; 
// import ModalBody from '../modal/ModalBody.jsx';
// import './Modal.scss'; 
// import ModalHeader from './ModalHeader.jsx';
// import ModalWrapper from './ModalWrapper.jsx';

// function ModalImage({ product, onClose, onAddToCart }) {
//   const handleAddToCartClick = () => {
//     onAddToCart(); // Вызываем функцию добавления в корзину
//     onClose();
//   };

//   return (
    
    
//     <ModalWrapper  onClose={onClose}>

    
//     <div className="modal">
//       <div className='div-ModalClose'>
//         <ModalClose onClose={onClose} /> 
//       </div>
//       <ModalHeader imageUrl={product.image} />
//       <div className="modal-content">
//         <ModalBody>
//           <h2 className='modal-body1'>Добавить продукт в корзину?</h2>
//         </ModalBody>
//       </div>
//       <ModalFooter 
//         firstText="Добавить"
//         secondText="Отмена"
//         firstClick={handleAddToCartClick} 
//         secondClick={onClose} 
//       />
//     </div>
//     </ModalWrapper>
    
//   );
// }

// ModalImage.propTypes = {
//   product: PropTypes.object.isRequired,
//   onClose: PropTypes.func.isRequired,
//   onAddToCart: PropTypes.func.isRequired, 
// };

// export default ModalImage;




import React from 'react';
import PropTypes from 'prop-types';
import ModalFooter from '../modal/ModalFooter.jsx';
import ModalClose from '../modal/ModalClose.jsx'; 
import ModalBody from '../modal/ModalBody.jsx';
import './Modal.scss'; 
import ModalHeader from './ModalHeader.jsx';
import ModalWrapper from './ModalWrapper.jsx';
import { useLocation } from 'react-router-dom';

function ModalImage({ product, onClose, onAddToCart }) {
  const location = useLocation()
 console.log(location.pathname)

 
  const handleAddToCartClick = () => {
    onAddToCart(); // Вызываем функцию добавления в корзину
    onClose();
  };
 
  return (
    <ModalWrapper onClose={onClose}>
      <div className="modal">
        <div className='div-ModalClose'>
          <ModalClose onClose={onClose} /> 
        </div>
        <ModalHeader imageUrl={product.image} />
        <div className="modal-content">
          <ModalBody>
            <h2 className='modal-body1'>Добавить продукт в корзину?</h2>
          </ModalBody>
        </div>
        <ModalFooter 
          firstText="Добавить"
          secondText="Отмена"
          firstClick={handleAddToCartClick} 
          secondClick={onClose} 
          closeModal={onClose}
        />
      </div>
    </ModalWrapper>
  );
}

ModalImage.propTypes = {
  product: PropTypes.object.isRequired,
 
  onClose: PropTypes.func.isRequired,
  onAddToCart: PropTypes.func.isRequired, 
};

export default ModalImage;
