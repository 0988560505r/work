// CartCounter.jsx
import React from 'react';
import PropTypes from 'prop-types';
import './Counter.scss';
import ModalWrapper from '../modal/ModalWrapper';

function CartCounter({ count }) {
  return (
   <div className='block-div w'>
    <div className="cart-counter">
      <span className='spanBacketLike'><img src="./free.png" alt="backet" /> {count}</span>
    </div>
    </div>
  );
}

CartCounter.propTypes = {
  count: PropTypes.number.isRequired,
};

export default CartCounter;
