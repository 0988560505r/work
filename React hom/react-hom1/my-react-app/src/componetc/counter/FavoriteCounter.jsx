// FavoriteCounter.jsx
import React from 'react';
import PropTypes from 'prop-types';
import './Counter.scss';
import ModalWrapper from '../modal/ModalWrapper';
function FavoriteCounter({ count }) {
  return (
    <div className='block-div q'>
    <div className="favorite-counter">
      <span className='spanBacketLike'><img src="like.png" alt="like" /> {count}</span>
    </div>
    </div>
  );
}

FavoriteCounter.propTypes = {
  count: PropTypes.number.isRequired,
};

export default FavoriteCounter;
