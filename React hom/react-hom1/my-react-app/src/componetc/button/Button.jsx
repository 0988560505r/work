import React, { useState } from 'react';
import PropTypes from "prop-types";
// import './Button.scss';

const Button = (props) => {
    const {
        className,
        type,
        onClick,
        children,
        
    } = props

    return (
        <button
        className={`buttonMain x  ${className}`}
            type={type}
            onClick={onClick}
            
        >
            {children}
        </button>
    )
}

Button.defaultProps = {
    type: "button"
}

Button.propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.any,
    restProps: PropTypes.object,
}

export default Button