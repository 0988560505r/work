import React from 'react';
import { Link } from 'react-router-dom';
import PageCart from '../../pages/PageCart';
import './style.scss';

const Header = () => {
  return (
    <nav>
      <ul className='ulHederLink'>
        <li><Link to="/" className="nav-link-cart">Home</Link></li>
        <li><Link to="/cart"  className="nav-link-cart">Cart</Link></li>
        <li><Link to="/favorite" className="nav-link-cart">Favorites</Link></li>
      </ul>
    </nav>
  );
};

export default Header;
