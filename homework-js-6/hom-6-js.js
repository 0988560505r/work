"use strict"
function createNewUser() {
    let firstName = prompt("Укажите ваше имя:", "Имя");
    let lastName = prompt("Укажите вашу фамилию:", "Фамилия");
    let birthday = prompt("Укажите вашу дату рождения (в формате dd.mm.yyyy):", "дд.мм.рррр");
  
    let newUser = {
      firstName: firstName,
      lastName: lastName,
      birthday: birthday,
      getAge: function() {
        let today = new Date();
        let birthDate = new Date(this.birthday.split('.').reverse().join('-'));
        let age = today.getFullYear() - birthDate.getFullYear();
        let monthDiff = today.getMonth() - birthDate.getMonth();
        
        if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDate.getDate())) {
          age--;
        }
        
        return age;
      },
      getPassword: function() {
        let firstLetter = this.firstName.charAt(0).toUpperCase();
        let lowercaseLastName = this.lastName.toLowerCase();
        let birthYear = this.birthday.split('.')[2];
        
        return firstLetter + lowercaseLastName + birthYear;
      }
    };
  
    return newUser;
  }
  
  let user = createNewUser();
  console.log(user);
  console.log("Age: " + user.getAge());
  console.log("Password: " + user.getPassword());
  