const gulp = require('gulp');
const concat = require('gulp-concat');

gulp.task("moveCss", function(){
    return gulp.src("src/css/*.css")
        .pipe(concat("all.css"))
        .pipe(gulp.dest("dist/css"));
});
gulp.task("moveHtml", function(){
    return gulp.src("src/html/*.html")
        .pipe(gulp.dest("dist/"));
});

gulp.task("moveImages", function(){
    return gulp.src("src/images/*.*")
        .pipe(gulp.dest("dist/images"));
});
gulp.task("style",() => {
    return gulp.src("src/sass/**/*.scss")
    .pipe(sass().on('error',sass.logError))
    .pipe(gulp.dest('./dist/css'));
});
gulp.task("watch", () => {
    gulp.watch('./src/sass/**/*.scss')
    .on('change',gulp.series("styles"))
});
gulp.task("dev", gulp.series("moveHtml","moveCss","moveImages"));